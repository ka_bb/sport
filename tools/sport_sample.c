/* */
#include <stdio.h>
#include <argp.h>
#include <pthread.h>
#include <unistd.h>

#include "sport.h"

#define SAMPLE_WR_TASK_NUM	(2)
#define SAMPLE_RD_TASK_TIMEOUT	(1000)

const char *argp_program_version =
  "sport_sample 1.0";
const char *argp_program_bug_address =
  "<kamal.aliev@icloud.com>";

/* Program documentation. */
static char doc[] =
  "SPORT example";

/* A description of the arguments we accept. */
static char args_doc[] = "NAME PATH";

/* The options we understand. */
static struct argp_option options[] = {
	{"list",    'l', 0,      0,  "List known ports" },
	{ 0 }
};

/* Used by main to communicate with parse_opt. */
struct arguments {
	char *args[2];
	bool list_ports;
};

struct sample_ctx {
	struct sport_port *port;
	struct sport_instance *instance;

	struct wr_task_info {
		pthread_t handle;
		struct {
			uint32_t id;
			uint32_t period;
			struct sport_instance *instance;
		} ctx;
	} wr_task[SAMPLE_WR_TASK_NUM];

	struct rd_task_info {
		pthread_t handle;
	} rd_task;
} ctx = {
	.port = NULL,
	.instance = NULL,
};

/* Parse a single option. */
static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
	struct arguments *arguments = state->input;

	switch (key) {
	case 'l':
		arguments->list_ports = true;
		break;
	case ARGP_KEY_ARG:
		if (state->arg_num >= 2) {
			/* Too many arguments. */
			argp_usage(state);
		}
		arguments->args[state->arg_num] = arg;
		break;
	case ARGP_KEY_END:
		if (state->arg_num < 2) {
			/* Not enough arguments. */
			argp_usage(state);
		}
		break;
	default:
		return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };

void *wr_task(void *p)
{
	enum sport_status status;
	struct wr_task_info *task_info = (struct wr_task_info *)p;
	char frame[32];
	uint32_t frame_count = 0;

	printf("WR task %u started...\n", task_info->ctx.id);

	while (true) {
		snprintf(frame, sizeof(frame), "task %u sample frame %u", task_info->ctx.id, frame_count++);

		/* write frame */
		status = sport_frame_write(task_info->ctx.instance, 0, 0,
					   (uint8_t *)frame, strlen(frame) + 1);
		if (status != SPORT_STATUS_SUCCESS)
			printf("task %u port frame '%s' write failed, status=%d\n", task_info->ctx.id, frame, status);

		sleep(task_info->ctx.period);
	}

	return NULL;
}

void *rd_task(void *p)
{
	enum sport_status status;
	struct sport_instance *instance = (struct sport_instance *)p;
	uint8_t frame[SPORT_FRAME_LENGHTH_MAX];
	uint32_t frame_len;

	printf("RD task started...\n");

	while (true) {
		memset(frame, 0, sizeof(frame));
		frame_len = 0;

		/* read port frame */
		status = sport_frame_read(instance, 0, SAMPLE_RD_TASK_TIMEOUT,
				   	  sizeof(frame), frame,
				   	  &frame_len);
		if (status == SPORT_STATUS_SUCCESS) {
			printf("port read %u bytes frame '%s'\n", frame_len, frame);
		} else if (status != SPORT_STATUS_ERROR_TIMEOUT) {
			printf("port read failed, status=%d\n", status);
		}
	}
}

int main(int argc, char *argv[])
{
	enum sport_status status;
	int pthread_ret;
	uint32_t i;
	struct arguments arguments = {0};
	struct sport_module_options module_options = {
		.port_max = 2,
	};
	struct sport_options port_options;

	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	printf("using '%s' device at '%s'\n", arguments.args[0], arguments.args[1]);

	/* init SPORT module */
	status = sport_module_init(&module_options);
	if (status != SPORT_STATUS_SUCCESS) {
		printf("SPORT module initialization failed, status=%d\n", status);
		return -1;
	}

	/* probe port */
	status = sport_port_probe(arguments.args[0], arguments.args[1],
				  &port_options);
	if (status != SPORT_STATUS_SUCCESS) {
		printf("port probe failed, status=%d\n", status);
		return -1;
	}

	/* create port */
	status = sport_port_create(arguments.args[0], arguments.args[1],
				   &port_options, &ctx.port);
	if (status != SPORT_STATUS_SUCCESS) {
		printf("port create failed, status=%d\n", status);
		return -1;
	}

	/* open port */
	status = sport_port_open(ctx.port, &ctx.instance);
	if (status != SPORT_STATUS_SUCCESS) {
		printf("port open failed, status=%d\n", status);
		return -1;
	}

	/* create Read Task */
	pthread_ret = pthread_create(&(ctx.rd_task.handle),
				     NULL, rd_task, (void *)ctx.instance);
	if (pthread_ret != 0) {
		printf("rd_task create failed, pthread_ret=%d\n", pthread_ret);
		return -1;
	}

	/* create Write Task(s) */
	for (i = 0; i < SAMPLE_WR_TASK_NUM; i++) {
		ctx.wr_task[i].ctx.id       = i;
		ctx.wr_task[i].ctx.period   = i + 1;
		ctx.wr_task[i].ctx.instance = ctx.instance;

		pthread_ret = pthread_create(&(ctx.wr_task[i].handle),
					     NULL, wr_task, (void *)&ctx.wr_task[i]);
		if (pthread_ret != 0) {
			printf("wr_task %u create failed, pthread_ret=%d\n", i, pthread_ret);
			return -1;
		}
	}

	while (true) {
		sleep(1);
	}

	return 0;
}

