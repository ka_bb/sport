/* */

#include "sport.h"
#include "sport_os.h"
#include "sport_dev.h"
#include "sport_frame.h"
#include "sport_common.h"

#define RD_BUFF_SIZE	(64)

/** */
struct sport_port {
	/** */
	const struct sport_dev *dev;
	/** Port device path */
	char path[SPORT_DEV_PATH_LENGTH_MAX];
	/** */
	struct sport_options options;
};

/** */
struct sport_instance {
	/** Port reference */
	struct sport_port *port;
	/** Instance file descriptor */
	int fd;
	/** Write operation lock  */
	sport_os_mutex_t wr_lock;
	/** Read operation info */
	struct {
		/**  */
		uint8_t buf[RD_BUFF_SIZE];
		/** */
		uint32_t buf_len;

		/* Decoded frame (length: CRC32 + Frame Length) */
		uint8_t frame[sizeof(uint32_t) + SPORT_FRAME_LENGHTH_MAX];
		struct sport_frame_decode_state decode_state;
	} rd_info;
};

/** Module context */
static struct module_ctx {
	/** Module options */
	struct sport_module_options options;
	/** Module ports */
	struct sport_port **port_pool;
	/** */
	uint32_t port_count;
	/** Context lock */
	sport_os_mutex_t lock;
} *ctx = NULL;

static uint32_t port_count_get(struct module_ctx *p_ctx)
{
	return p_ctx->port_count;
}

static struct sport_port *port_find(struct module_ctx *p_ctx,
				    const char *name,
				    const char *path)
{
	struct sport_port *port = NULL;
	uint32_t i;

	/* find port by its name &  path */
	for (i = 0; i < p_ctx->options.port_max; i++) {
		if (p_ctx->port_pool[i] == NULL)
			continue;

		if ((strcasecmp(name, p_ctx->port_pool[i]->dev->name) == 0) &&
		    (strcasecmp(path, p_ctx->port_pool[i]->path) == 0)) {
			port = p_ctx->port_pool[i];
			break;
		}
	}

	return port;
}

static struct sport_port *port_add(struct module_ctx *p_ctx,
				   struct sport_port *port)
{
	struct sport_port *p = NULL;
	uint32_t i;

	if (p_ctx->port_count >= p_ctx->options.port_max)
		return p;

	/* find port by its name &  path */
	for (i = 0; i < p_ctx->options.port_max; i++) {
		if (p_ctx->port_pool[i])
			continue;

		p_ctx->port_count++;
		p_ctx->port_pool[i] = p = port;
		break;

	}

	return p;
}

/* Function parameters & description please find under 'sport.h' */
enum sport_status sport_module_init(const struct sport_module_options *options)
{
	enum sport_status status;

	SPORT_CHECK((ctx != NULL), SPORT_STATUS_ERROR_INITIALIZED);

	/* allocate module context */
	ctx = (struct module_ctx *)malloc(sizeof(*ctx));
	if (ctx == NULL) {
		sport_dbg_err("module context allocate failed" SPORT_CRLF);
		return SPORT_STATUS_ERROR_NOT_FOUND;
	}

	/* reset context data */
	memset(ctx, 0, sizeof(*ctx));

	/* allocate ports pool */
	ctx->port_pool = (struct sport_port **)
			  malloc(sizeof(struct sport_port *) * options->port_max);
	if (ctx->port_pool == NULL) {
		sport_dbg_err("module port pool allocate failed" SPORT_CRLF);
		return SPORT_STATUS_ERROR_NOT_FOUND;
	}

	/* reset ports */
	memset(ctx->port_pool, 0, sizeof(struct sport_port *) * options->port_max);

	/* create context lock */
	status = sport_os_mutex_init(&ctx->lock);
	if (status != SPORT_STATUS_SUCCESS) {
		sport_dbg_err("module context lock create failed, status=%d" SPORT_CRLF, status);
		return status;
	}

	/* cache module options */
	memcpy(&ctx->options, options, sizeof(struct sport_module_options));

	return SPORT_STATUS_SUCCESS;
}

/* Function parameters & description please find under 'sport.h' */
enum sport_status sport_module_exit(void)
{

	return SPORT_STATUS_ERROR_NOT_IMPLEMENTED;
}

/* Function parameters & description please find under 'sport.h' */
enum sport_status sport_port_probe(const char *name,
				   const char *path,
				   struct sport_options *options)
{
	enum sport_status lock_status, status;
	struct sport_port *p;

	SPORT_CHECK((name == NULL),    SPORT_STATUS_ERROR_NULL);
	SPORT_CHECK((path == NULL),    SPORT_STATUS_ERROR_NULL);
	SPORT_CHECK((options == NULL), SPORT_STATUS_ERROR_NULL);

	/* lock module */
	lock_status = sport_os_mutex_lock(&ctx->lock);
	SPORT_CHECK((lock_status != SPORT_STATUS_SUCCESS), lock_status);

	/* check if port was already created */
	p = port_find(ctx, name, path);
	if (p) {
		/* port exists, report current settings */
		memcpy(options, &p->options, sizeof(struct sport_options));
	} else {
		/* report default port settings */
		status = sport_dev_probe(name, path, options);
	}

	/* unlock module */
	lock_status = sport_os_mutex_unlock(&ctx->lock);
	SPORT_CHECK((lock_status != SPORT_STATUS_SUCCESS), lock_status);

	return status;
}

/* Function parameters & description please find under 'sport.h' */
enum sport_status sport_port_create(const char *name,
				    const char *path,
				    const struct sport_options *options,
				    struct sport_port **port)
{
	enum sport_status status  = SPORT_STATUS_SUCCESS;
	const struct sport_dev *dev;
	struct sport_port *p;

	SPORT_CHECK((ctx == NULL),     SPORT_STATUS_ERROR_NOT_INITIALIZED);
	SPORT_CHECK((name == NULL),    SPORT_STATUS_ERROR_NULL);
	SPORT_CHECK((path == NULL),    SPORT_STATUS_ERROR_NULL);
	SPORT_CHECK((options == NULL), SPORT_STATUS_ERROR_NULL);
	SPORT_CHECK((*port != NULL),   SPORT_STATUS_ERROR_PARAM);
	SPORT_CHECK((strlen(path) >= SPORT_DEV_PATH_LENGTH_MAX),
				       SPORT_STATUS_ERROR_PARAM);

	/* lock module */
	status = sport_os_mutex_lock(&ctx->lock);
	SPORT_CHECK((status != SPORT_STATUS_SUCCESS), status);

	do {
		/* check if new port can be added */
		if (port_count_get(ctx) >= ctx->options.port_max) {
			sport_dbg_err("port %s@%s create failed, no free pool mem" SPORT_CRLF, name, path);
			status = SPORT_STATUS_ERROR_OVFL;
			break;
		}

		/* check if port was already created */
		p = port_find(ctx, name, path);
		if (p != NULL) {
			sport_dbg_err("port %s@%s exists" SPORT_CRLF, name, path);
			status = SPORT_STATUS_ERROR_EXISTS;
			break;
		}

		/* find device by name */
		dev = sport_dev_find(name);
		if (dev == NULL) {
			sport_dbg_err("device %s find failed" SPORT_CRLF, name);
			status = SPORT_STATUS_ERROR_NOT_FOUND;
			break;
		}

		/* allocate port */
		p = (struct sport_port *)malloc(sizeof(struct sport_port));
		if (p == NULL) {
			sport_dbg_err("device %s port allocate failed" SPORT_CRLF, name);
			status = SPORT_STATUS_ERROR_MEM;
			break;
		}

		/* reset port data */
		memset(p, 0, sizeof(*p));

		/* cache newly created port */
		if (port_add(ctx, p) == NULL) {
			sport_dbg_err("port %s@%s add failed" SPORT_CRLF, name, path);
			status = SPORT_STATUS_ERROR_OVFL;
			break;
		}

		/* cache options */
		memcpy(&p->options, options, sizeof(*options));
		/* cache port device path */
		strcpy(p->path, path);
		/* cache device descriptor for a later use */
		p->dev = dev;
		/* return the create port */
		*port = p;
	} while (false);

	/* unlock module */
	status = sport_os_mutex_unlock(&ctx->lock);
	SPORT_CHECK((status != SPORT_STATUS_SUCCESS), status);

	return status;
}

/* Function parameters & description please find under 'sport.h' */
enum sport_status sport_port_destroy(struct sport_port **port)
{
	return SPORT_STATUS_ERROR_NOT_IMPLEMENTED;
}


/* Function parameters & description please find under 'sport.h' */
enum sport_status sport_port_open(struct sport_port *port,
				  struct sport_instance **instance)
{
	enum sport_status status = SPORT_STATUS_SUCCESS;
	struct sport_instance *ins = NULL;
	int fd;

	SPORT_CHECK((ctx == NULL),       SPORT_STATUS_ERROR_NOT_INITIALIZED);
	SPORT_CHECK((port == NULL),      SPORT_STATUS_ERROR_NULL);
	SPORT_CHECK((*instance != NULL), SPORT_STATUS_ERROR_PARAM);

	if (port->dev->ops.open) {
		/* open port device */
		fd = port->dev->ops.open(port->path, &port->options);
		if (fd <= 0) {
			sport_dbg_err("port %s@%s open failed" SPORT_CRLF, port->dev->name, port->path);
			status = SPORT_STATUS_ERROR_DEV_OPEN;
		} else {
			/* allocate port instance */
			ins = (struct sport_instance *)malloc(sizeof(*ins));
			if (ins == NULL) {
				sport_dbg_err("port %s@%s instance allocate failed" SPORT_CRLF, port->dev->name, port->path);
				status = SPORT_STATUS_ERROR_MEM;
			} else {
				/* reset instance data */
				memset(ins, 0, sizeof(*ins));

				/* create context lock */
				status = sport_os_mutex_init(&ins->wr_lock);
				if (status != SPORT_STATUS_SUCCESS) {
					sport_dbg_err("port WR lock create failed, status=%d" SPORT_CRLF, status);

					(void)sport_port_close(&ins);
					return status;
				}

				/* reset instance data */
				memset(ins, 0, sizeof(*ins));

				/* cache port reference */
				ins->port = port;
				/* cache instance fd */
				ins->fd = fd;
				/* return created instance */
				*instance = ins;
			}
		}
	} else {
		status = SPORT_STATUS_ERROR_DEV_OP_NOT_AVAILABLE;
	}

	return status;
}

/* Function parameters & description please find under 'sport.h' */
enum sport_status sport_port_close(struct sport_instance **instance)
{
	SPORT_CHECK((ctx == NULL), SPORT_STATUS_ERROR_NOT_INITIALIZED);

	return SPORT_STATUS_ERROR_NOT_IMPLEMENTED;
}

/* Function parameters & description please find under 'sport.h' */
enum sport_status sport_frame_read(struct sport_instance *instance,
				   const uint32_t lport,
				   const uint32_t timeout,
				   const uint32_t frame_len_max,
				   uint8_t *frame,
				   uint32_t *frame_len)
{
	enum sport_status status;
	uint32_t _timeout = timeout, rd_len, i, crc32_frame, crc32_calc;
	bool is_decoded = false;
	sport_time_t r_time, e_time;

	SPORT_CHECK((ctx == NULL),        SPORT_STATUS_ERROR_NOT_INITIALIZED);
	SPORT_CHECK((instance == NULL),   SPORT_STATUS_ERROR_NULL);
	SPORT_CHECK((frame == NULL),      SPORT_STATUS_ERROR_NULL);
	SPORT_CHECK((frame_len == NULL),  SPORT_STATUS_ERROR_NULL);
	SPORT_CHECK((frame_len_max == 0), SPORT_STATUS_ERROR_PARAM);
	SPORT_CHECK((instance->port->dev->ops.read == NULL),
					  SPORT_STATUS_ERROR_DEV_OP_NOT_AVAILABLE);


	/* get time reference */
	r_time = sport_os_elapsed_time_msec_get(0);
	while (true) {
		/* read port */
		status = instance->port->dev->ops.read(instance->fd, 0, _timeout,
						       sizeof(instance->rd_info.buf) - instance->rd_info.buf_len,
						       &(instance->rd_info.buf[instance->rd_info.buf_len]),
						       &rd_len);
		if (status == SPORT_STATUS_ERROR_TIMEOUT) {
			break;
		} else if (status != SPORT_STATUS_SUCCESS) {
			sport_frame_decode_reset(&instance->rd_info.decode_state);
			break;
		} else {
			instance->rd_info.buf_len += rd_len;

			for (i = 0; i < instance->rd_info.buf_len; i++) {
				/* decode received bytes into frame */
				status = sport_frame_decode(instance->rd_info.buf[i],
				     			    instance->rd_info.frame,
				     			    sizeof(instance->rd_info.frame),
				     			    &instance->rd_info.decode_state);
				if (status == SPORT_STATUS_SUCCESS) {
					if (instance->rd_info.decode_state.status == SPORT_FRAME_DECODE_STATUS_DONE) {
						if (instance->rd_info.decode_state.decoded < sizeof(crc32_frame)) {
							status = SPORT_STATUS_ERROR_SHORT_FRAME;
							break;
						}

						*frame_len = instance->rd_info.decode_state.decoded - sizeof(crc32_frame);

						if (*frame_len > frame_len_max) {
							status = SPORT_STATUS_ERROR_OVFL;
							break;
						}

						/* get frame CRC field */
						memcpy(&crc32_frame, instance->rd_info.frame, sizeof(crc32_frame));
						crc32_frame = SPORT_NET2HST(crc32_frame);

						/* calculate CRC over payload & check */
						crc32_calc = sport_frame_crc32_get(instance->rd_info.frame + sizeof(crc32_frame), *frame_len);
						if (crc32_frame != crc32_calc) {
							sport_dbg_err("frame crc mismatch (0x%08X vs. 0x%08X)" SPORT_CRLF, crc32_frame, crc32_calc);
							status = SPORT_STATUS_ERROR_CRC;
							break;
						}

						/* copy received frame to output */
						memcpy(frame, instance->rd_info.frame + sizeof(crc32_frame), *frame_len);

						/* frame decoded */
						is_decoded = true;
						break;
					}
				} else {
					sport_frame_decode_reset(&instance->rd_info.decode_state);
					break;
				}
			}

			/* cache undecoded portion for the next iteration */
			instance->rd_info.buf_len -= i;
			memmove(instance->rd_info.buf, &instance->rd_info.buf[i % sizeof(instance->rd_info.buf)], instance->rd_info.buf_len);

			if (is_decoded)
				break;

			if (status != SPORT_STATUS_SUCCESS)
				break;

			/* get time elapsed since reference */
			e_time = sport_os_elapsed_time_msec_get(r_time);
			if (e_time < timeout) {
				/* update timeot & continue */
				_timeout = timeout - e_time;
				continue;
			}
		}
	}

	return status;
}

static enum sport_status frame_on_output(const uint8_t *data,
					 const uint32_t data_len,
					 void *priv)
{
	struct sport_instance *instance = (struct sport_instance *)priv;

	return instance->port->dev->ops.write(instance->fd, 0, 0, data, data_len);
}

/* Function parameters & description please find under 'sport.h' */
enum sport_status sport_frame_write(struct sport_instance *instance,
				    const uint32_t lport,
				    const uint32_t timeout,
				    const uint8_t *frame,
				    const uint32_t frame_len)
{
	enum sport_status lock_status, status;
	uint32_t frame_crc32;

	SPORT_UNUSED_PARAM(timeout);

	SPORT_CHECK((ctx == NULL),      SPORT_STATUS_ERROR_NOT_INITIALIZED);
	SPORT_CHECK((instance == NULL), SPORT_STATUS_ERROR_NULL);
	SPORT_CHECK((frame == NULL),    SPORT_STATUS_ERROR_NULL);
	SPORT_CHECK((frame_len > SPORT_FRAME_LENGHTH_MAX),
				        SPORT_STATUS_ERROR_PARAM);

	/* lock write operation */
	lock_status = sport_os_mutex_lock(&instance->wr_lock);
	SPORT_CHECK((lock_status != SPORT_STATUS_SUCCESS), lock_status);

	do {
		if (instance->port->dev->ops.write) {
			/* get frame CRC32 */
			frame_crc32 = SPORT_HST2NET(sport_frame_crc32_get(frame, frame_len));

			/* open frame */
			status = sport_frame_encode(SPORT_FRAME_ENCODE_ACTION_OPEN,
						    NULL, 0,
						    frame_on_output, (void *)instance);
			if (status != SPORT_STATUS_SUCCESS) {
				sport_dbg_err("frame encode(open) failed, status=%d" SPORT_CRLF, status);
				break;
			}

			/* encode frame CRC32 */
			status = sport_frame_encode(SPORT_FRAME_ENCODE_ACTION_INNER,
						    (uint8_t *)&frame_crc32, sizeof(frame_crc32),
						    frame_on_output, (void *)instance);
			if (status != SPORT_STATUS_SUCCESS) {
				sport_dbg_err("frame encode failed, status=%d" SPORT_CRLF, status);
				break;
			}

			/* encode frame Payload */
			status = sport_frame_encode(SPORT_FRAME_ENCODE_ACTION_INNER,
						    frame, frame_len,
						    frame_on_output, (void *)instance);
			if (status != SPORT_STATUS_SUCCESS) {
				sport_dbg_err("frame encode failed, status=%d" SPORT_CRLF, status);
				break;
			}

			/* close frame */
			status = sport_frame_encode(SPORT_FRAME_ENCODE_ACTION_CLOSE,
						    NULL, 0,
						    frame_on_output, (void *)instance);
			if (status != SPORT_STATUS_SUCCESS) {
				sport_dbg_err("frame encode(open) failed, status=%d" SPORT_CRLF, status);
				break;
			}
		} else {
			status = SPORT_STATUS_ERROR_DEV_OP_NOT_AVAILABLE;
		}
	} while (false);

	/* unlock write operation */
	lock_status = sport_os_mutex_unlock(&instance->wr_lock);
	SPORT_CHECK((lock_status != SPORT_STATUS_SUCCESS), lock_status);

	return status;
}

