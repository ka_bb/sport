/* */
#ifndef _SPORT_OS_H_
#define _SPORT_OS_H_

#include "sport_types.h"

/** \defgroup SPORT OS - OS abstractions

   @{
*/

#if defined(LINUX) || defined(__LINUX__)
# include <pthread.h>
# include <arpa/inet.h>
# include <sys/time.h>
#else
# error "please define target OS type"
#endif


#if defined(LINUX) || defined(__LINUX__)
# define SPORT_HST2NET(X)	htonl((X))
# define SPORT_NET2HST(X)	ntohl((X))

typedef pthread_mutex_t sport_os_mutex_t;
typedef time_t sport_time_t;
#endif

/**
*/
enum sport_status sport_os_mutex_init(sport_os_mutex_t *mutex);

/**
*/
enum sport_status sport_os_mutex_lock(sport_os_mutex_t *mutex);

/**
*/
enum sport_status sport_os_mutex_unlock(sport_os_mutex_t *mutex);

/**
*/
sport_time_t sport_os_elapsed_time_msec_get(const sport_time_t ref_time);

/** @} */

#endif

