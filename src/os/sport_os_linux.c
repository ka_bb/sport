/**/
#include "sport_os.h"

/* Function parameters & description please find under 'sport_os.h' */
enum sport_status sport_os_mutex_init(sport_os_mutex_t *mutex)
{
	return (pthread_mutex_init(mutex, NULL) != 0) ? SPORT_STATUS_ERROR_OS:
							SPORT_STATUS_SUCCESS;
}


/* Function parameters & description please find under 'sport_os.h' */
enum sport_status sport_os_mutex_lock(sport_os_mutex_t *mutex)
{
	return (pthread_mutex_lock(mutex) != 0) ? SPORT_STATUS_ERROR_OS:
						  SPORT_STATUS_SUCCESS;
}

/* Function parameters & description please find under 'sport_os.h' */
enum sport_status sport_os_mutex_unlock(sport_os_mutex_t *mutex)
{
	return (pthread_mutex_unlock(mutex) != 0) ? SPORT_STATUS_ERROR_OS:
						    SPORT_STATUS_SUCCESS;
}

/* Function parameters & description please find under 'sport_os.h' */
sport_time_t sport_os_elapsed_time_msec_get(const sport_time_t ref_time)
{
	struct timeval tv;
	sport_time_t curr_time_ms = 0;

	memset(&tv, 0, sizeof(tv));
	gettimeofday(&tv, NULL);

	curr_time_ms = (sport_time_t)(tv.tv_sec*1000 + (tv.tv_usec) / 1000);

	if ( (ref_time == 0) || (ref_time > curr_time_ms) )
		return curr_time_ms;

	return (curr_time_ms - ref_time);
}

