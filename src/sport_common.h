/* */
#ifndef _SPORT_COMMON_H_
#define _SPORT_COMMON_H_

#include "sport_types.h"
#include "sport_debug.h"

#ifdef HAVE_CONFIG_H
# include "sport_config.h"
#endif

/** \defgroup SPORT_COMMON SPORT - Common Definitions & Helpers

   @{
*/

#ifndef ARRAY_SIZE
# define ARRAY_SIZE(X)		(sizeof((X)) / sizeof((X)[0]))
#endif

#define SPORT_UNUSED_PARAM(X)	((void)(X))

#define SPORT_CHECK(_CONDITION_, _STATUS_) \
	do { \
		if ((_CONDITION_)) { \
			sport_dbg_err("%s(), line %d (%s), status=%d\r\n", \
					    __FUNCTION__, __LINE__, \
					    #_CONDITION_, (_STATUS_)); \
			return _STATUS_; \
		} \
	} while (0)

/** @} */

#endif
