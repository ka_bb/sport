/* */
#ifndef _SPORT_FRAME_H_
#define _SPORT_FRAME_H_

#include "sport_types.h"

/** \defgroup SPORT_FRAME SPORT - Frame operations

   @{
*/

/** Enumeration to define frame decoding status */
enum sport_frame_decode_status {
	/** Sync  */
	SPORT_FRAME_DECODE_STATUS_SYNC = 0,
	/** Progress */
	SPORT_FRAME_DECODE_STATUS_PROG = 1,
	/** Replacement */
	SPORT_FRAME_DECODE_STATUS_REPL = 2,
	/** Decoding done */
	SPORT_FRAME_DECODE_STATUS_DONE = 3,
	/** Decoding error  */
	SPORT_FRAME_DECODE_STATUS_FAIL = 4,
};

/** Enumeration to define encode action */
enum sport_frame_encode_action {
	/** */
	SPORT_FRAME_ENCODE_ACTION_OPEN = 0,
	/** */
	SPORT_FRAME_ENCODE_ACTION_INNER = 1,
	/** */
	SPORT_FRAME_ENCODE_ACTION_CLOSE = 2,
};

/** Structure to define frame decoding state */
struct sport_frame_decode_state {
	/** Decode status */
	enum sport_frame_decode_status status;
	/** Number of decoded bytes */
	uint32_t decoded;
};

/** */
typedef enum sport_status (*sport_frame_output_t)(const uint8_t *data,
						  const uint32_t data_len,
						  void *priv);

/** Reset frame decoding state

    \param[in,out] state  decoder state

    \return None
*/
void sport_frame_decode_reset(struct sport_frame_decode_state *state);

/** Encode Frame

    \param[in] action       encode action
    \param[in] frame        frame data
    \param[in] frame_len    frame data length (bytes)
    \param[in] output       encoded frame data output handler
    \param[in] output_priv  output handler private data

    \return \ref SPORT_STATUS_SUCCESS in case of SUCCESS, ERROR otherwise
*/
enum sport_status sport_frame_encode(const enum sport_frame_encode_action action,
				     const uint8_t *frame,
				     const uint32_t frame_len,
				     const sport_frame_output_t output,
				     void *output_priv);

/** Decode into Frame

    \param[in]     byte           byte to decode into frame
    \param[out]    frame          decoded frame
    \param[in]     frame_len_max  maximum decoded frame length (bytes)
    \param[in,out] state          frame decoding state

    \return \ref SPORT_STATUS_SUCCESS in case of SUCCESS, ERROR otherwise
*/
enum sport_status sport_frame_decode(const uint8_t byte,
			             uint8_t *frame,
			             const uint32_t frame_len_max,
			             struct sport_frame_decode_state *state);

/** Get CRC32

    \param[in] data      data
    \param[in] data_len  data length (bytes)

    \return calculated CRC32
*/
uint32_t sport_frame_crc32_get(const uint8_t *data,
			       const uint32_t data_len);

/** @} */

#endif
