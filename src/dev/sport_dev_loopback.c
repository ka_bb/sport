/* */
#include "sport_dev.h"
#include "sport_common.h"


static int dev_loopback_open(const char *path,
			     const struct sport_options *options)
{
	SPORT_UNUSED_PARAM(path);
	SPORT_UNUSED_PARAM(options);
	return -1;
}

static int dev_loopback_close(const int fd)
{
	SPORT_UNUSED_PARAM(fd);
	return -1;
}

static enum sport_status dev_loopback_read(const int fd,
					   const uint32_t lport,
					   const uint32_t timeout,
					   const uint32_t data_len_max,
					   uint8_t *data,
					   uint32_t *data_len)
{
	SPORT_UNUSED_PARAM(fd);
	SPORT_UNUSED_PARAM(lport);
	SPORT_UNUSED_PARAM(timeout);
	SPORT_UNUSED_PARAM(data_len_max);
	SPORT_UNUSED_PARAM(data);
	SPORT_UNUSED_PARAM(data_len);

	return SPORT_STATUS_ERROR_NOT_IMPLEMENTED;
}

static enum sport_status dev_loopback_write(const int fd,
					    const uint32_t lport,
					    const uint32_t timeout,
					    const uint8_t *data,
					    const uint32_t data_len)
{
	SPORT_UNUSED_PARAM(fd);
	SPORT_UNUSED_PARAM(lport);
	SPORT_UNUSED_PARAM(timeout);
	SPORT_UNUSED_PARAM(data);
	SPORT_UNUSED_PARAM(data_len);

	return SPORT_STATUS_ERROR_NOT_IMPLEMENTED;
}

const struct sport_dev sport_dev_loopback = {
	.name = "loopback",
	.ops  = {
		.open    = dev_loopback_open,
		.close   = dev_loopback_close,
		.read    = dev_loopback_read,
		.write   = dev_loopback_write,
	},
	.opt = {
		.type = SPORT_TYPE_LOOPBACK,
		.data = {
			.loopback = {
				.dummy = 0,
			},
		},
	},
};

