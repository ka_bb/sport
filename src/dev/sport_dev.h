/* */
#ifndef _SPORT_DEV_H_
#define _SPORT_DEV_H_

#include "sport.h"

/** \defgroup SPORT_DEV SPORT - DEV API

   @{
*/

/** Structure to define device operations */
struct sport_dev_operations {
	/* open device */
	int (*open)(const char *path,
		    const struct sport_options *options);
	/* clode device */
	int (*close)(const int fd);
	/* read frame */
	enum sport_status (*read)(const int fd,
				  const uint32_t lport,
				  const uint32_t timeout,
				  const uint32_t data_len_max,
				  uint8_t *data,
				  uint32_t *data_len);
	/* write frame */
	enum sport_status (*write)(const int fd,
				   const uint32_t lport,
				   const uint32_t timeout,
				   const uint8_t *data,
				   const uint32_t data_len);
};


/** Structure to define device */
struct sport_dev {
	/** Device name */
	const char *name;
	/** Device operations*/
	struct sport_dev_operations ops;
	/** Device options */
	struct sport_options opt;
};

/**
*/
enum sport_status sport_dev_probe(const char *name,
				  const char *path,
				  struct sport_options *options);

/**
*/
const struct sport_dev *sport_dev_find(const char *name);

/** @} */

#endif
