/* */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/select.h>

#include "sport_dev.h"
#include "sport_common.h"

static int dev_uart_linux_open(const char *path,
			       const struct sport_options *options)
{
	int fd;
	struct termios termios_options;
	speed_t speed;

	fd = open(path, O_RDWR | O_NOCTTY | O_NDELAY);
	if (fd == -1) {
		/* Could not open the port. */
		sport_dbg_err("port at %s open failed" SPORT_CRLF, path);
	} else {
		/* set non blocking mode */
		fcntl(fd, F_SETFL, FNDELAY);
	}

	if (tcgetattr(fd, &termios_options) != 0) {
		sport_dbg_err("port at %s tcgetattr() failed" SPORT_CRLF, path);
		close(fd);
		return -1;
	}

	switch (options->data.uart.speed) {
	case SPORT_UART_SPEED_9600:
		speed = B9600;
		break;
	case SPORT_UART_SPEED_19200:
		speed = B19200;
		break;
	case SPORT_UART_SPEED_38400:
		speed = B38400;
		break;
	case SPORT_UART_SPEED_57600:
		speed = B57600;
		break;
	case SPORT_UART_SPEED_115200:
		speed = B115200;
		break;
	default:
		sport_dbg_err("port at %s unsupported speed, setting 115200 by default" SPORT_CRLF, path);
		speed = B115200;
		break;
	}

	if (cfsetispeed(&termios_options, speed) != 0) {
		sport_dbg_err("port at %s cfsetispeed() failed" SPORT_CRLF, path);
		close(fd);
		return -1;
	}

	if (cfsetospeed(&termios_options, speed) != 0) {
		sport_dbg_err("port at %s cfsetospeed() failed" SPORT_CRLF, path);
		close(fd);
		return -1;
	}

        termios_options.c_cflag = (termios_options.c_cflag & ~CSIZE) | CS8;     /* 8-bit chars */
        termios_options.c_iflag &= ~(IGNBRK | INLCR | IGNCR | ICRNL);
        termios_options.c_lflag = 0;                /* no signaling chars, no echo, */
                                        	    /* no canonical processing */
        termios_options.c_oflag = 0;                /* no remapping, no delays */
        termios_options.c_cc[VMIN]  = 0;            /* read doesn't block */

        termios_options.c_iflag &= ~(IXON | IXOFF | IXANY); /* shut off xon/xoff ctrl */

        termios_options.c_cflag |= (CLOCAL | CREAD);/* ignore modem controls, */
                                        /* enable reading */
        termios_options.c_cflag &= ~(PARENB | PARODD);      /* shut off parity */
        termios_options.c_cflag &= ~CSTOPB;
        termios_options.c_cflag &= ~CRTSCTS;

        if (tcsetattr (fd, TCSANOW, &termios_options) != 0) {
		sport_dbg_err("port at %s tcsetattr() failed" SPORT_CRLF, path);
		close(fd);
                return -1;
        }

	return fd;
}

static int dev_uart_linux_close(const int fd)
{
	return close(fd);
}

static enum sport_status dev_uart_linux_read(const int fd,
					     const uint32_t lport,
					     const uint32_t timeout,
					     const uint32_t data_len_max,
					     uint8_t *data,
					     uint32_t *data_len)
{
	enum sport_status status = SPORT_STATUS_SUCCESS;
	int n, max_fd = fd + 1;
	fd_set input;
	struct timeval s_timeout;

	SPORT_UNUSED_PARAM(lport);

	/* Initialize the input set */
	FD_ZERO(&input);
	FD_SET(fd, &input);

	/* Initialize the timeout structure */
	s_timeout.tv_sec  =  timeout / 1000;
	s_timeout.tv_usec = (timeout % 1000) * 1000;

	/* wait on select */
	n = select(max_fd, &input, NULL, NULL, &s_timeout);
	if (n < 0) {
		status = SPORT_STATUS_ERROR_DEV_READ;
	} else if (n == 0) {
		status = SPORT_STATUS_ERROR_TIMEOUT;
	} else {
		n = read(fd, data, data_len_max);
		if (n < 0) {
			status = SPORT_STATUS_ERROR_DEV_READ;
		} else {
			*data_len = (uint32_t)n;
		}
	}

	return status;
}

static enum sport_status dev_uart_linux_write(const int fd,
					      const uint32_t lport,
					      const uint32_t timeout,
					      const uint8_t *data,
					      const uint32_t data_len)
{
	int sent;

	SPORT_UNUSED_PARAM(lport);
	SPORT_UNUSED_PARAM(timeout);

	sent = write(fd, data, data_len);

	return (sent != data_len) ? SPORT_STATUS_ERROR_DEV_WRITE :
				    SPORT_STATUS_SUCCESS;
}

const struct sport_dev sport_dev_uart_linux = {
	.name = "uart_linux",
	.ops  = {
		.open    = dev_uart_linux_open,
		.close   = dev_uart_linux_close,
		.read    = dev_uart_linux_read,
		.write   = dev_uart_linux_write,
	},
	.opt = {
		.type = SPORT_TYPE_UART,
		.data = {
			.uart = {
				.speed = SPORT_UART_SPEED_115200,
			},
		},
	},
};
