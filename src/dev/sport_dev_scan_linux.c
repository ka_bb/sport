/* */
#include <pthread.h>

#include "sport_dev.h"
#include "sport_common.h"

static int dev_scan_linux_open(const char *path,
			       const struct sport_options *options)
{
	SPORT_UNUSED_PARAM(path);
	SPORT_UNUSED_PARAM(options);
	return -1;
}

static int dev_scan_linux_close(const int fd)
{
	SPORT_UNUSED_PARAM(fd);
	return -1;
}

static enum sport_status dev_scan_linux_read(const int fd,
					     const uint32_t lport,
					     const uint32_t timeout,
					     const uint32_t data_len_max,
					     uint8_t *data,
					     uint32_t *data_len)
{
	SPORT_UNUSED_PARAM(fd);
	SPORT_UNUSED_PARAM(lport);
	SPORT_UNUSED_PARAM(timeout);
	SPORT_UNUSED_PARAM(data_len_max);
	SPORT_UNUSED_PARAM(data);
	SPORT_UNUSED_PARAM(data_len);

	return SPORT_STATUS_ERROR_NOT_IMPLEMENTED;
}

static enum sport_status dev_scan_linux_write(const int fd,
					      const uint32_t lport,
					      const uint32_t timeout,
					      const uint8_t *data,
					      const uint32_t data_len)
{
	SPORT_UNUSED_PARAM(fd);
	SPORT_UNUSED_PARAM(lport);
	SPORT_UNUSED_PARAM(timeout);
	SPORT_UNUSED_PARAM(data);
	SPORT_UNUSED_PARAM(data_len);

	return SPORT_STATUS_ERROR_NOT_IMPLEMENTED;
}

const struct sport_dev sport_dev_scan_linux = {
	.name = "scan_linux",
	.ops  = {
		.open    = dev_scan_linux_open,
		.close   = dev_scan_linux_close,
		.read    = dev_scan_linux_read,
		.write   = dev_scan_linux_write,
	},
	.opt = {
		.type = SPORT_TYPE_CAN,
		.data = {
			.can = {
				.speed = SPORT_CAN_SPEED_500K,
			},
		},
	},
};

