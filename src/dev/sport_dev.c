/**/
#include "sport_dev.h"
#include "sport_common.h"

extern const struct sport_dev sport_dev_loopback;
#ifdef SPORT_ENABLE_DEV_UART_LINUX
extern const struct sport_dev sport_dev_uart_linux;
#endif
#ifdef SPORT_ENABLE_DEV_SCAN_LINUX
extern const struct sport_dev sport_dev_scan_linux;
#endif

/* pool of available devices */
static const struct sport_dev *dev_pool[] = {
	&sport_dev_loopback,
#ifdef SPORT_ENABLE_DEV_UART_LINUX
	&sport_dev_uart_linux,
#endif
#ifdef SPORT_ENABLE_DEV_SCAN_LINUX
	&sport_dev_scan_linux,
#endif
};

static const struct sport_dev *dev_find(const char *name)
{
	const struct sport_dev *dev = NULL;
	uint32_t i;

	/* find device by the given name */
	for (i = 0; i < ARRAY_SIZE(dev_pool); i++) {
		if (strcasecmp(name, dev_pool[i]->name) == 0) {
			dev = dev_pool[i];
			break;
		}
	}

	return dev;
}

/* Function parameters & description please find under 'sport_dev.h' */
enum sport_status sport_dev_probe(const char *name,
				  const char *path,
				  struct sport_options *options)
{
	const struct sport_dev *dev = NULL;

	SPORT_UNUSED_PARAM(path);

	/* lookup device by name */
	dev = dev_find(name);
	if (dev == NULL)
		return SPORT_STATUS_ERROR_NOT_FOUND;

	/** \todo do real device probe with the given device 'path'
	*/

	/* get device options  */
	memcpy(options, &dev->opt, sizeof(struct sport_options));

	return SPORT_STATUS_SUCCESS;
}

/* Function parameters & description please find under 'sport_dev.h' */
const struct sport_dev *sport_dev_find(const char *name)
{
	/* lookup device by name */
	return dev_find(name);
}

