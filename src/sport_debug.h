/* */
#ifndef _SPORT_DEBUG_H_
#define _SPORT_DEBUG_H_

#include "sport_types.h"

/** \defgroup SPORT_DEBUG SPORT - Debug Definitions & Helpers

   @{
*/

/** \todo make this OS dependent
*/
#define SPORT_CRLF	"\n"

/** Output debug with error level */
#define sport_dbg_err	printf
/** Output debug with warning level */
#define sport_dbg_wrn	printf
/** Output debug with info level */
#define sport_dbg_inf	printf

/** @} */

#endif
