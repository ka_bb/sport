/* */
#ifndef _SPORT_TYPES_H_
#define _SPORT_TYPES_H_

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

/** \defgroup SPORT_TYPES SPORT - Common Types

   @{
*/

/** Enumeration to define status codes */
enum sport_status {
	/** Status Code to indicate Success */
	SPORT_STATUS_SUCCESS = 0,
	/** Status Code to indicate General Error */
	SPORT_STATUS_ERROR = -1,
	/** Status Code to indicate that resoiurce and/or data was not found */
	SPORT_STATUS_ERROR_NOT_FOUND = -2,
	/** Status Code to indicate NULL pointer */
	SPORT_STATUS_ERROR_NULL = -3,
	/** Status Code to indicate not implemented functionality */
	SPORT_STATUS_ERROR_NOT_IMPLEMENTED = -4,
	/** Status Code to indicate invalid parameter error */
	SPORT_STATUS_ERROR_PARAM = -5,
	/** Status Code to indicate data overflow error */
	SPORT_STATUS_ERROR_OVFL = -6,
	/** Status Code to indicate that the requested resource exists */
	SPORT_STATUS_ERROR_EXISTS = -7,
	/** Status Code to indicate memory allocation error */
	SPORT_STATUS_ERROR_MEM = -8,
	/** Status Code to indicate that the requested resource was initialized */
	SPORT_STATUS_ERROR_INITIALIZED = -9,
	/** Status Code to indicate that the requested resource was not initialized */
	SPORT_STATUS_ERROR_NOT_INITIALIZED = -10,
	/** Status Code to indicate general OS error */
	SPORT_STATUS_ERROR_OS = -11,
	/** Status Code to indicate timeout error */
	SPORT_STATUS_ERROR_TIMEOUT = -12,
	/** Status Code to indicate CRC mismatch error */
	SPORT_STATUS_ERROR_CRC = -13,
	/** Status Code to indicate short frame error */
	SPORT_STATUS_ERROR_SHORT_FRAME = -14,
	/** Status Code to indicate general device error */
	SPORT_STATUS_ERROR_DEV = -100,
	/** Status Code to indicate unavailable device operation */
	SPORT_STATUS_ERROR_DEV_OP_NOT_AVAILABLE = -101,
	/** Status Code to indicate failed device open operation */
	SPORT_STATUS_ERROR_DEV_OPEN = -102,
	/** Status Code to indicate failed device write operation */
	SPORT_STATUS_ERROR_DEV_WRITE = -103,
	/** Status Code to indicate failed device read operation */
	SPORT_STATUS_ERROR_DEV_READ = -104,
};

/** @} */

#endif
