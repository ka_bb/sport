/* */
#ifndef _SPORT_H_
#define _SPORT_H_

#include "sport_types.h"

/** \defgroup SPORT SPORT - Main API

   @{
*/

/** Maximum frame length (bytes) */
#define SPORT_FRAME_LENGHTH_MAX		(128UL)

/** */
#define SPORT_DEV_PATH_LENGTH_MAX	(32)

/** */
struct sport_port;

/** */
struct sport_instance;

/** Structure to define module options */
struct sport_module_options {
	/** Maximum number of supported ports */
	uint32_t port_max;
};

/* Enumeration to define port type */
enum sport_type {
	/** Not available */
	SPORT_TYPE_NA = -1,
	/** Loopback port */
	SPORT_TYPE_LOOPBACK = 0,
	/** UART port */
	SPORT_TYPE_UART = 1,
	/** CAN port */
	SPORT_TYPE_CAN = 2,
};

/** Structure to define loopback port options */
struct sport_options_loopback {
	uint8_t dummy;
};

/** Enumeration to define UART port speed */
enum sport_uart_speed {
	/** Speed not available */
	SPORT_UART_SPEED_NA = -1,
	/** 9600 bit/s */
	SPORT_UART_SPEED_9600 = 0,
	/** 19200 bit/s */
	SPORT_UART_SPEED_19200 = 1,
	/** 38400 bit/s */
	SPORT_UART_SPEED_38400 = 2,
	/** 57600 bit/s */
	SPORT_UART_SPEED_57600 = 3,
	/** 115200 bit/s */
	SPORT_UART_SPEED_115200 = 4,
};

/** Structure to define UART port options */
struct sport_options_uart {
	/** Speed */
	enum sport_uart_speed speed;
};

/** Enumeration to define CAN port speed */
enum sport_can_speed {
	/** Speed not available */
	SPORT_CAN_SPEED_NA = -1,
	/** 50 kbit/s */
	SPORT_CAN_SPEED_50K = 0,
	/** 100 kbit/s */
	SPORT_CAN_SPEED_100K = 1,
	/** 125 kbit/s */
	SPORT_CAN_SPEED_125K = 2,
	/** 250 kbit/s */
	SPORT_CAN_SPEED_250K = 3,
	/** 500 kbit/s */
	SPORT_CAN_SPEED_500K = 4,
	/** 1000 kbit/s */
	SPORT_CAN_SPEED_1000K = 5,
};

/** Structure to define UART port options */
struct sport_options_can {
	/** Speed */
	enum sport_can_speed speed;
};

/** Structure to define port options */
struct sport_options {
	/** Port Type */
	enum sport_type type;
	/** */
	union sport_unified_options {
		/** Loopback port options */
		struct sport_options_loopback loopback;
		/** UART port options */
		struct sport_options_uart uart;
		/** CAN port options */
		struct sport_options_can can;
	} data;
};

/** Initialize module

    \param[in] options  module options

    \return \ref SPORT_STATUS_SUCCESS in case of SUCCESS, ERROR otherwise
*/
enum sport_status sport_module_init(const struct sport_module_options *options);

/** De-initialize module

    \return \ref SPORT_STATUS_SUCCESS in case of SUCCESS, ERROR otherwise
*/
enum sport_status sport_module_exit(void);

/** Probe port

    \param[in]  name     port name
    \param[in]  path     port path
    \param[out] options  port options

    \return \ref SPORT_STATUS_SUCCESS in case of SUCCESS, ERROR otherwise
*/
enum sport_status sport_port_probe(const char *name,
				   const char *path,
				   struct sport_options *options);

/** Create port

    \param[in]  name     port name
    \param[in]  path     port path
    \param[in]  options  port options
    \param[out] port     created port

    \return \ref SPORT_STATUS_SUCCESS in case of SUCCESS, ERROR otherwise
*/
enum sport_status sport_port_create(const char *name,
				    const char *path,
				    const struct sport_options *options,
				    struct sport_port **port);

/** Destroy port

    \param[in] port port to destroy

    \return \ref SPORT_STATUS_SUCCESS in case of SUCCESS, ERROR otherwise
*/
enum sport_status sport_port_destroy(struct sport_port **port);

/** Open port

    \param[in]  port      port to open
    \param[out] instance  opened port instance

    \return \ref SPORT_STATUS_SUCCESS in case of SUCCESS, ERROR otherwise
*/
enum sport_status sport_port_open(struct sport_port *port,
				  struct sport_instance **instance);

/** Close port

    \param[in] instance  port instance to close

    \return \ref SPORT_STATUS_SUCCESS in case of SUCCESS, ERROR otherwise
*/
enum sport_status sport_port_close(struct sport_instance **instance);

/** Read frame

    \param[in]  instance       port instance
    \param[in]  lport          logical port identifier
    \param[in]  timeout        read timeout (ms)
    \param[in]  frame_len_max  maximum frame length to read
    \param[out] frame          read frame data
    \param[out] frame_len      read frame length (bytes)

    \return \ref SPORT_STATUS_SUCCESS in case of SUCCESS, ERROR otherwise
*/
enum sport_status sport_frame_read(struct sport_instance *instance,
				   const uint32_t lport,
				   const uint32_t timeout,
				   const uint32_t frame_len_max,
				   uint8_t *frame,
				   uint32_t *frame_len);

/** Write frame

    \param[in] instance       port instance
    \param[in] lport          logical port identifier
    \param[in] timeout        read timeout (ms)
    \param[in] frame          read frame data
    \param[in] frame_len      read frame length (bytes)

    \return \ref SPORT_STATUS_SUCCESS in case of SUCCESS, ERROR otherwise
*/
enum sport_status sport_frame_write(struct sport_instance *instance,
				    const uint32_t lport,
				    const uint32_t timeout,
				    const uint8_t *frame,
				    const uint32_t frame_len);

/** @} */

#endif

